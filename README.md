# Simple Cycles #

### What is this repository for? ###

This code tests three approaches to finding all simple cycles (cycles with no repeated nodes) in an undirected graph. The motivation is a [question](https://or.stackexchange.com/questions/9786/number-of-hamiltonian-sub-cycles-on-the-graph) on Operations Research Stack Exchange. The three approaches are tested on the nine node grid graph from the question.

### What are the approaches? ###

The first approach is a simple iterative scheme, in which paths start with an edge, are extended by indicident edges, and stop growing when an edge is added that forms a complete cycle. This is the most efficient approach computationally.

The second approach, based on a request in the original question, is a mixed integer linear program (MILP) model that finds all simple cycles. This is the slowest approach computationally.

The third approach exploits the solution pool feature in CPLEX. It uses a MILP model for finding a single cycle, combined with the `populate` method in CPLEX for filling the solution pool. While not guaranteed to find all solutions (and prone to finding the same solution repeatedly), it does succeed on the test graph.

### Details ###

The code was developed in Java using CPLEX 22.1.1 but should run with any version recent enough to have the solution pool feature. The first (iterative) approach is self-contained and does not require any outside software, so if that is all that interests you, you can delete all the code related to CPLEX and run just that.

The MILP formulations and information about the iterative method are contained in a sequence of blog posts, starting with [this post](https://orinanobworld.blogspot.com/2023/01/finding-all-simple-cycles-i.html).

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

