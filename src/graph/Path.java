package graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Path is a container for a path.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Path {
  private final ArrayList<Integer> nodes;  // the list of nodes in the path
  private int anchor;                      // the node starting the path
  private int terminus;                    // the current terminus of the path
  private boolean complete;                // true if the path is a cycle
  private boolean sorted;                  // true if the path has been sorted

  /**
   * Constructs an empty path.
   *
   * @param start the node where the path starts
   */
  public Path(final int start) {
    anchor = start;
    terminus = start;
    nodes = new ArrayList<>();
    nodes.add(start);
    complete = false;
    sorted = false;
  }

  /**
   * Copy constructor.
   * @param p the path to copy
   */
  public Path(final Path p) {
    anchor = p.getAnchor();
    terminus = p.getTerminus();
    complete = p.isCycle();
    nodes = new ArrayList<>(p.getPath());
  }

  /**
   * Appends an edge to the path.
   *
   * @param e the edge to append
   * @throws IllegalArgumentException if the edge is not incident to
   * the current terminus of the path or it is incident at an intermediate
   * node in the path
   * @throws IllegalStateException if the path is already a complete cycle
   */
  public void append(final Edge e) throws IllegalStateException {
    // Do not allow an edge to be added to a complete cycle.
    if (complete) {
      throw new IllegalStateException("Cannot add an edge"
                                      + " to a complete cycle.");
    }
    // Verify that the edge connects to the current path.
    if (!e.incidentAt(terminus)) {
      throw new IllegalArgumentException("Attempting to add an edge"
                                         + " not incident to the path.");
    }
    // Verify that the edge does not create a cycle within the path.
    int n = e.opposite(terminus);
    if (n != anchor && nodes.contains(n)) {
      throw new IllegalArgumentException("Edge creates a cycle"
                                         + " within the parent path.");
    }
    // Append the edge and update the terminus.
    terminus = e.opposite(terminus);
    nodes.add(terminus);
    // Check for completion.
    complete = (terminus == anchor);
  }

  /**
   * Reorganizes a complete cycle so that the anchor has the smallest index
   * of any node on the cycle and the index of the first node after leaving
   * the anchor is less than the index of the last node before returning to
   * the anchor.
   *
   * @throws IllegalStateException if the cycle is incomplete
   */
  public void reorder() throws IllegalStateException {
    // Skip if already sorted.
    if (!sorted) {
      if (!complete) {
        throw new IllegalStateException("Cannot reorder an incomplete cycle.");
      }
      // Rotate the cycle to make the smallest node the anchor.
      int a = nodes.stream().mapToInt(i->i).min().getAsInt();
      if (nodes.get(0) != a) {
        // Drop the first element (first occurrence of the old anchor).
        nodes.remove(0);
        // Find the new anchor.
        int ix = nodes.indexOf(a);
        // Rotate the new anchor into the starting position.
        Collections.rotate(nodes, -ix);
        // Append the new anchor at the end and record it as the new anchor.
        nodes.add(a);
        anchor = a;
      }
      // Sort the cycle so that the node after start is smaller than the
      // node immediately before return.
      if (nodes.get(1) > nodes.get(nodes.size() - 2)) {
        Collections.reverse(nodes);
      }
      sorted = true;
    }
  }

  /**
   * Displays the cycle as a string.
   * @return a string representation of the cycle
   */
  @Override
  public String toString() {
    if (!complete) {
      return "incomplete cycle";
    } else {
      reorder();
      return nodes.toString();
    }
  }

  /**
   * Gets the anchor.
   * @return the anchor node
   */
  public int getAnchor() {
    return anchor;
  }

  /**
   * Gets the sequence of nodes in the path.
   * @return the node sequence
   */
  public List<Integer> getPath() {
    return Collections.unmodifiableList(nodes);
  }

  /**
   * Gets the current terminus of the path.
   * @return the terminus
   */
  public int getTerminus() {
    return terminus;
  }

  /**
   * Tests whether the path is a cycle.
   * @return true if the path forms a cycle
   */
  public boolean isCycle() {
    return complete;
  }

  /**
   * Gets the number of edges in the current path.
   * @return the edge count
   */
  public int getLength() {
    return nodes.size() - 1;
  }

  /**
   * Tests whether this path is equivalent to another one.
   * The test applies only to complete cycles.
   * @param other the other path
   * @return true if the paths are equivalent
   * @throws IllegalStateException if either path is not a complete cycle.
   */
  public boolean equivalentTo(final Path other) throws IllegalStateException {
    // Check for full cycles.
    if (!complete || !other.isCycle()) {
      throw new IllegalStateException("Equivalence is limited to full cycles.");
    }
    // Both arguments should be sorted.
    this.reorder();
    other.reorder();
    // Check for identical node sequences.
    return nodes.equals(other.getPath());
  }
}
