package graph;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Graph is a container for the components of an undirected graph.
 *
 * Vertices are numbered starting from 1.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Graph {
  private final int nNodes;           // number of nodes
  private final HashSet<Edge> edges;  // the set of all edges
  private final HashMap<Integer, HashSet<Edge>> incidence;
    // node - edge incidence map

  /**
   * Creates a graph with a specified node set and no edges.
   * @param n the number of nodes (nodes will be numbered 1 .. n)
   */
  public Graph(final int n) {
    nNodes = n;
    edges = new HashSet<>();
    incidence = new HashMap<>();
    for (int i = 1; i <= n; i++) {
      incidence.put(i, new HashSet<>());
    }
  }

  /**
   * Creates an edge between two nodes and adds the edge to the graph if not
   * already present. If the edge is already present, it is ignored.
   * @param a the first vertex
   * @param b the second vertex
   */
  public void addEdge(final int a, final int b) {
    Edge e = new Edge(a, b);
    if (!edges.contains(e)) {
      edges.add(e);
      incidence.get(a).add(e);
      incidence.get(b).add(e);
    }
  }

  /**
   * Gets the number of nodes.
   * @return the number of nodes
   */
  public int getNodeCount() {
    return nNodes;
  }

  /**
   * Gets the set of edges incident at a specified node.
   * @param n the node
   * @return the set of incident edges
   */
  public Set<Edge> incidentAt(final int n) {
    return Collections.unmodifiableSet(incidence.get(n));
  }

  /**
   * Gets the edges of the graph as a matrix with two columns (tail, head).
   * @return the matrix of edges
   */
  public int[][] getEdgeMatrix() {
    int[][] matrix = new int[edges.size()][2];
    int index = 0;
    for (Edge e : edges) {
      matrix[index][0] = e.getTail();
      matrix[index][1] = e.getHead();
      index += 1;
    }
    return matrix;
  }
}
