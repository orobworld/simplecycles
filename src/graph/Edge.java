package graph;

/**
 * Edge represents an undirected edge in a graph.
 *
 * Edges contain a "tail" node and a "head" node, but the designation of which
 * is tail and which is head is arbitrary since edges are undirected.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Edge {
  private final int tail;  // tail node
  private final int head;  // head node

  /**
   * Constructor.
   * @param a the index of the first vertex
   * @param b the index of the second vertex
   */
  public Edge(final int a, final int b) {
    tail = Math.min(a, b);
    head = Math.max(a, b);
  }

  /**
   * Expresses the edge as a string.
   * @return a string representing the edge
   */
  @Override
  public String toString() {
    return "[" + tail + ", " + head + "]";
  }

  /**
   * Finds the endpoint of the edge opposite a specified endpoint.
   * @param v the index of the specified endpoint
   * @return the index of the opposite endpoint
   * @throws IllegalArgumentException if the edge does not contain the
   * specified endpoint
   */
  public int opposite(final int v) throws IllegalArgumentException {
    if (v == tail) {
      return head;
    } else if (v == head) {
      return tail;
    } else {
      throw new IllegalArgumentException("The vertex is not on the edge.");
    }
  }

  /**
   * Gets the tail node.
   * @return the tail node
   */
  public int getTail() {
    return tail;
  }

  /**
   * Gets the head node.
   * @return the head node
   */
  public int getHead() {
    return head;
  }

  /**
   * Tests whether an edge is incident at a node.
   * @param n the node index
   * @return true if the node is an endpoint of the edge
   */
  public boolean incidentAt(final int n) {
    return (n == head || n == tail);
  }
}
