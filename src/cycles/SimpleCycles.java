package cycles;

import graph.Path;
import graph.Graph;
import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import java.util.List;

/**
 * SimpleCycles tests an iterative algorithm for finding all simple cycles
 * in an undirected graph.
 *
 * The motivation is a question on OR Stack Exchange:
 * https://or.stackexchange.com/questions/9786/number-of-hamiltonian-sub
 * -cycles-on-the-graph
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SimpleCycles {

  /**
   * Dummy constructor.
   */
  private SimpleCycles() { }

  /**
   * Builds a test graph and enumerates all simple cycles.
   * @param args the command line arguments (ignored)
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Build the graph in the OR SE post.
    int nNodes = 9;
    int[][] edges = new int[][] {
      {1, 2}, {1, 6}, {2, 3}, {2, 5}, {3, 4}, {4, 5}, {4, 7}, {5, 6}, {5, 8},
      {6, 9}, {7, 8}, {8, 9}
    };
    Graph graph = new Graph(nNodes);
    for (int[] e : edges) {
      graph.addEdge(e[0], e[1]);
    }
    // Determine the full set of simple cycles using the iterative procedure.
    System.out.println("Running the iterative cycle finder ...");
    long time = System.currentTimeMillis();
    CycleFinder finder = new CycleFinder(graph);
    int n = finder.findCycles();
    System.out.println("A total of " + n + " simple cycles were found:");
    for (Path c : finder.getCycles()) {
      System.out.println(c);
    }
    System.out.println("Iterative constructor run time = "
                       + (System.currentTimeMillis() - time) + " ms.");
    // Try the all-cycles MIP model.
    double timeLimit = 60;   // time limit in seconds
    boolean asym = true;     // add asymmetry constraints?
    int maxCycles = 20;      // upper bound on the cycle count
    System.out.println("\nRunning the all-cycles MIP model ...");
    try (AllCyclesMIP mip = new AllCyclesMIP(graph, maxCycles, asym)) {
      IloCplex.Status status = mip.solve(timeLimit);
      System.out.println("\nMIP solver status = " + status);
      System.out.println("The model found " + mip.getCycleCount() + " cycles:");
      for (Path c : mip.getCycles()) {
        System.out.println(c);
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
    // Try the single-cycle MIP model using the solution pool.
    int popSize = 200;  // population size limit, inflated due to duplicates
    System.out.println("\nRunning the single cycle MIP model ...");
    try (SingleCycleMIP mip = new SingleCycleMIP(graph)) {
      int found = mip.solve(timeLimit, popSize);
      System.out.println("\nThe solution pool contains = "
                         + found + " cycles.");
      List<Path> cycles = mip.getCycles();
      System.out.println(cycles.size() + " unique cycles were found:");
      for (Path c : cycles) {
        System.out.println(c);
      }
    } catch (IloException ex) {
      System.out.println(ex.getMessage());
    }
  }

}
