package cycles;

import graph.Path;
import graph.Edge;
import graph.Graph;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * CycleFinder locates all simple cycles in a graph.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CycleFinder {
  private final Graph graph;              // the graph to parse
  private final ArrayList<Path> cycles;   // the cycles found

  /**
   * Constructor.
   * @param g the graph to parse
   */
  public CycleFinder(final Graph g) {
    graph = g;
    cycles = new ArrayList<>();
  }

  /**
   * Finds all simple cycles in the graph.
   * @return the number of cycles found
   */
  public int findCycles() {
    int n = graph.getNodeCount();
    for (int v = 1; v < n; v++) {
      // Find all cycles starting with vertex v and containing no
      // vertices with lower indices.
      findCyclesFrom(v);
    }
    return cycles.size();
  }

  /**
   * Finds all cycles with a given starting node that do not contain
   * any nodes with lower indices.
   * @param anchor the starting node
   */
  private void findCyclesFrom(final int anchor) {
    // Create a queue of paths to expand.
    LinkedList<Path> queue = new LinkedList<>(); // paths to work on
    ArrayList<Path> found = new ArrayList<>();   // cycles found so far
    Path p = new Path(anchor);
    queue.push(p);
    // Try to expand each path in the queue.
    while (!queue.isEmpty()) {
      // Pop the next path to work on.
      p = queue.pop();
      int length = p.getLength();
      // Try adding any qualifying edge incident to its terminus.
      int terminus = p.getTerminus();
      for (Edge e : graph.incidentAt(terminus)) {
        int n = e.opposite(terminus);
        // Ignore the edge if it is incident to a forbidden node or if it
        // creates a "cycle" of length 2.
        if (n > anchor || (n == anchor && length > 1)) {
          // Clone the current path and try to extend it.
          Path p2 = new Path(p);
          try {
            p2.append(e);
            // If the result is a complete cycle, check to see if it is a
            // duplicate and, if not, store it. If not a cycle, queue the
            // result.
            if (p2.isCycle()) {
              p2.reorder();
              if (!contains(found, p2)) {
                found.add(p2);
              }
            } else {
              queue.push(p2);
            }
          } catch (IllegalStateException | IllegalArgumentException ex) {
            // Ignore any thrown exceptions and just abort the extension.
          }
        }
      }
    }
    // Store all the cycles found.
    cycles.addAll(found);
  }

  /**
   * Gets a copy of the list of cycles found.
   * @return the cycle list
   */
  public List<Path> getCycles() {
    return Collections.unmodifiableList(cycles);
  }

  /**
   * Tests whether a path is already contained in a list of paths.
   * @param list the list of existing paths
   * @param path the path to test
   * @return true if the path is already in the list
   */
  private boolean contains(final List<Path> list, final Path path) {
    for (Path p : list) {
      if (p.equivalentTo(path)) {
        return true;
      }
    }
    return false;
  }

}
