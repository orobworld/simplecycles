package cycles;

import graph.Edge;
import graph.Graph;
import graph.Path;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.List;

/**
 * AllCyclesMIP implements a mixed integer programming model to find all
 * simple cycles in a graph.
 *
 * Note: Node indices start at 1 rather than 0, which is reflected in the
 * indexing of the variables.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class AllCyclesMIP implements AutoCloseable {
  private static final double HALF = 0.5; // for rounding binary variables
  private final Graph graph;              // the graph to solve

  // Dimensions
  private final int maxCycles;            // an upper bound on the cycle count
  private final int maxNode;               // the number of nodes in the graph

  // CPLEX objects
  private final IloCplex mip;             // the MIP model
  private final IloNumVar[] build;        // indicators for cycles to build
  private final IloNumVar[][] useVertex;  // indicators for vertex inclusion
  private final IloNumVar[][][] useEdge;  // indicators for edge inclusion
  private final IloNumVar[][] anchor;     // indicators for cycle anchors
  private final IloNumVar[][][][] differ; // indicators for edge differences
                                          // between cycles
  private final IloNumVar[][][] flow;     // MTZ "flow" variables

  /**
   * Constructor.
   * @param g the graph to solve
   * @param cycles an upper bound on the number of cycles
   * @param asym if true, add asymmetry constraints
   * @throws IloException if the model cannot be constructed
   */
  public AllCyclesMIP(final Graph g, final int cycles, final boolean asym)
                      throws IloException {
    graph = g;
    maxNode = graph.getNodeCount() + 1;  // +1 because node indices start at 1
    maxCycles = cycles;
    int[][] edges = graph.getEdgeMatrix();
    // Initialize the model.
    mip = new IloCplex();
    // Create the variables.
    build = new IloNumVar[maxCycles];
    useVertex = new IloNumVar[maxNode][maxCycles];
    useEdge = new IloNumVar[maxNode][maxNode][maxCycles];
    anchor = new IloNumVar[maxNode][maxCycles];
    differ = new IloNumVar[maxNode][maxNode][maxCycles][maxCycles];
    flow = new IloNumVar[maxNode][maxNode][maxCycles];
    for (int c = 0; c < maxCycles; c++) {
      build[c] = mip.boolVar("use_cycle_" + c);
      for (int i = 1; i < maxNode; i++) {
        useVertex[i][c] = mip.boolVar("useVertex_" + i + "_c_" + c);
        anchor[i][c] = mip.boolVar("anchor_" + i + "_" + c);
        for (int j = 1; j < maxNode; j++) {
          useEdge[i][j][c] = mip.boolVar("useEdge_" + i + "_" + j + "_c_" + c);
          flow[i][j][c] =
            mip.numVar(0, maxNode, "flow_" + i + "_" + j + "_" + c);
          for (int c0 = 0; c0 < maxCycles; c0++) {
            differ[i][j][c][c0] =
              mip.boolVar("differ_" + i + "_" + j + "_" + c + "_" + c0);
          }
        }
      }
    }
    // Objective: maximize the number of cycles built.
    mip.addMaximize(mip.sum(build));
    // Constraint: vertices can only belong to constructed cycles.
    for (int c = 0; c < maxCycles; c++) {
      for (int i = 1; i < maxNode; i++) {
        mip.addLe(useVertex[i][c], build[c]);
      }
    }
    // Constraint: edges can only belong to cycles containing both endpoints,
    // and only in one orientation.
    for (int c = 0; c < maxCycles; c++) {
      for (int[] e : edges) {
        int i = e[0];
        int j = e[1];
        mip.addLe(mip.sum(useEdge[i][j][c], useEdge[j][i][c]),
                  useVertex[i][c]);
        mip.addLe(mip.sum(useEdge[i][j][c], useEdge[j][i][c]),
                  useVertex[j][c]);
      }
    }
    // Constraint: flow only occurs on edges that are used, and only in the
    // orientation used.
    for (int c = 0; c < maxCycles; c++) {
      for (int[] e : edges) {
        int i = e[0];
        int j = e[1];
        mip.addLe(flow[i][j][c],
                  mip.prod(maxNode, useEdge[i][j][c]));
        mip.addLe(flow[j][i][c],
                  mip.prod(maxNode, useEdge[j][i][c]));
      }
    }
    // Constraint: used cycles have one anchor and unused cycles have none.
    for (int c = 0; c < maxCycles; c++) {
      IloLinearNumExpr expr = mip.linearNumExpr();
      for (int i = 1; i < maxNode; i++) {
        expr.addTerm(1.0, anchor[i][c]);
      }
      mip.addEq(expr, build[c]);
    }
    // Constraint: anchors must belong to their cycles.
    for (int c = 0; c < maxCycles; c++) {
      for (int i = 1; i < maxNode; i++) {
        mip.addLe(anchor[i][c], useVertex[i][c]);
      }
    }
    // Constraint: for every cycle/node combo, either two edges (if the node
    // belongs to the cycle) or zero (if not) are incident.
    for (int c = 0; c < maxCycles; c++) {
      for (int i = 1; i < maxNode; i++) {
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (Edge e : graph.incidentAt(i)) {
          int j = e.opposite(i);
          expr.addTerm(1.0, useEdge[i][j][c]);
          expr.addTerm(1.0, useEdge[j][i][c]);
        }
        mip.addEq(expr, mip.prod(2, useVertex[i][c]));
      }
    }
    // Constraint: in any used cycle, at any node in the cycle other than the
    // anchor, flow out is at least one unit less than flow in.
    for (int c = 0; c < maxCycles; c++) {
      for (int i = 1; i < maxNode; i++) {
        IloLinearNumExpr left = mip.linearNumExpr();
        IloLinearNumExpr right = mip.linearNumExpr();
        for (Edge e : graph.incidentAt(i)) {
          int j = e.opposite(i);
          left.addTerm(1.0, flow[i][j][c]);
          right.addTerm(1.0, flow[j][i][c]);
        }
        right.addTerm(-1.0, useVertex[i][c]);
        right.addTerm(maxNode, anchor[i][c]);
        mip.addLe(left, right);
      }
    }
    // Constraint: in any used cycle, at least one unit of flow must return
    // to the anchor node (to ensure a complete cycle).
    for (int c = 0; c < maxCycles; c++) {
      for (int i = 1; i < maxNode; i++) {
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (Edge e : graph.incidentAt(i)) {
          int j = e.opposite(i);
          expr.addTerm(1.0, flow[j][i][c]);
        }
        mip.addGe(expr, anchor[i][c]);
      }
    }
    // Constraint: define the `differ` variables.
    for (int c = 0; c < maxCycles; c++) {
      for (int c0 = c + 1; c0 < maxCycles; c0++) {
        for (int[] e : edges) {
          int i = e[0];
          int j = e[1];
          IloLinearNumExpr expr = mip.linearNumExpr();
          expr.addTerm(1.0, useEdge[i][j][c]);
          expr.addTerm(1.0, useEdge[i][j][c0]);
          expr.addTerm(1.0, useEdge[j][i][c]);
          expr.addTerm(1.0, useEdge[j][i][c0]);
          mip.addLe(differ[i][j][c][c0], expr);
          mip.addLe(differ[i][j][c][c0], mip.diff(2.0, expr));
        }
      }
    }
    // Constraint: any pair of cycles must differ in at least one edge.
    for (int c = 0; c < maxCycles; c++) {
      for (int c0 = c + 1; c0 < maxCycles; c0++) {
        IloLinearNumExpr expr = mip.linearNumExpr();
        for (int[] e : edges) {
          int i = e[0];
          int j = e[1];
          expr.addTerm(1.0, differ[i][j][c][c0]);
        }
        mip.addGe(expr, mip.diff(mip.sum(build[c], build[c0]), 1.0));
      }
    }
    // Constraint (asymmetry): used cycles precede unusued ones.
    for (int c = 1; c < maxCycles; c++) {
      mip.addLe(build[c], build[c - 1]);
    }
    // Add more antisymmetry constraints if requested.
    if (asym) {
      // Constraint: the anchor in a cycle must have the smallest index of
      // any node in the cycle.
      for (int c = 0; c < maxCycles; c++) {
        for (int i = 2; i < maxNode; i++) {
          for (int j = 1; j < i; j++) {
            mip.addLe(mip.sum(anchor[i][c], useVertex[j][c]), 1.0);
          }
        }
      }
    }
  }

  /**
   * Solves the MIP model.
   * @param timeLimit the time limit in seconds
   * @return the final solver status
   * @throws IloException if anything goes splat
   */
  public IloCplex.Status solve(final double timeLimit) throws IloException {
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    mip.solve();
    return mip.getStatus();
  }

  /**
   * Gets the number of cycles found.
   * @return the cycle count
   * @throws IloException if CPLEX does not have a solution
   */
  public long getCycleCount() throws IloException {
    return Math.round(mip.getObjValue());
  }

  /**
   * Gets the cycles found by the model.
   * @return a list of cycles found
   * @throws IloException if CPLEX cannot recover the solution
   */
  public List<Path> getCycles() throws IloException {
    ArrayList<Path> cycles = new ArrayList<>();
    // Look for cycles that were built.
    for (int c = 0; c < maxCycles; c++) {
      if (mip.getValue(build[c]) > HALF) {
        // Find the anchor node.
        int a = -1;
        for (int i = 1; i < maxNode; i++) {
          if (mip.getValue(anchor[i][c]) > HALF) {
            a = i;
            break;
          }
        }
        // Start a path at the anchor.
        Path path = new Path(a);
        int i = a;
        // Trace the path until it returns to the anchor.
        while (!path.isCycle()) {
          // Follow the flow to the next node.
          for (Edge e : graph.incidentAt(i)) {
            int j = e.opposite(i);
            if (mip.getValue(flow[i][j][c]) > HALF) {
              // Append the edge and update the current position.
              path.append(new Edge(i, j));
              i = j;
              // Check whether the path is now a cycle.
              break;
            }
          }
        }
        // Add the completed cycle to the output.
        cycles.add(path);
      } else {
        // If a cycle is not built, neither is any higher-index cycle.
        break;
      }
    }
    return cycles;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    mip.close();
  }

}
