package cycles;

import graph.Edge;
import graph.Graph;
import graph.Path;
import ilog.concert.IloException;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.ArrayList;
import java.util.List;

/**
 * SingleCycleMIP uses a mixed integer program to find a single simple cycle
 * in a graph.
 *
 * It can be combined with either the CPLEX solution pool or progressive
 * addition of no-good constraints to try to find all simple cycles.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class SingleCycleMIP implements AutoCloseable {
  private static final double HALF = 0.5; // for rounding binary variables
  private static final int INTENSITY = 4; // CPLEX maximum intensity setting
  private final Graph graph;              // the graph to solve

  // Dimensions
  private final int maxNode;              // the number of nodes in the graph

  // CPLEX objects
  private final IloCplex mip;             // the MIP model
  private final IloNumVar[] useVertex;    // indicators for vertex inclusion
  private final IloNumVar[][] useEdge;    // indicators for edge inclusion
  private final IloNumVar[] anchor;       // indicators for cycle anchor
  private final IloNumVar[][] flow;       // MTZ "flow" variables

  /**
   * Constructor.
   * @param g the graph to solve
   * @throws IloException if CPLEX cannot build the model
   */
  public SingleCycleMIP(final Graph g) throws IloException {
    graph = g;
    maxNode = graph.getNodeCount() + 1;  // +1 because node indices start at 1
    int[][] edges = graph.getEdgeMatrix();
    // Initialize the model.
    mip = new IloCplex();
    // Create the variables.
    useVertex = new IloNumVar[maxNode];
    useEdge = new IloNumVar[maxNode][maxNode];
    anchor = new IloNumVar[maxNode];
    flow = new IloNumVar[maxNode][maxNode];
    for (int i = 1; i < maxNode; i++) {
      useVertex[i] = mip.boolVar("useVertex_" + i);
      anchor[i] = mip.boolVar("anchor_" + i);
      for (int j = 1; j < maxNode; j++) {
        useEdge[i][j] = mip.boolVar("useEdge_" + i + "_" + j);
        flow[i][j] = mip.numVar(0, maxNode, "flow_" + i + "_" + j);
      }
    }
    // We can use the default objective function (minimize 0) since we just
    // want a feasible solution.
    //
    // Constraint: an edge can only belong to a cycle containing both endpoints,
    // and only in one orientation.
    for (int[] e : edges) {
      int i = e[0];
      int j = e[1];
      mip.addLe(mip.sum(useEdge[i][j], useEdge[j][i]), useVertex[i]);
      mip.addLe(mip.sum(useEdge[i][j], useEdge[j][i]), useVertex[j]);
    }
    // Constraint: flow only occurs on edges that are used, and only in the
    // orientation used.
    for (int[] e : edges) {
      int i = e[0];
      int j = e[1];
      mip.addLe(flow[i][j], mip.prod(maxNode, useEdge[i][j]));
      mip.addLe(flow[j][i], mip.prod(maxNode, useEdge[j][i]));
    }
    // Constraint: the cycle has exactly one anchor.
    IloLinearNumExpr expr = mip.linearNumExpr();
    for (int i = 1; i < maxNode; i++) {
      expr.addTerm(1.0, anchor[i]);
    }
    mip.addEq(expr, 1.0);
    // Constraint: the anchors must belong to the cycle.
    for (int i = 1; i < maxNode; i++) {
      mip.addLe(anchor[i], useVertex[i]);
    }
    // Constraint: at every node, either two edges (if the node
    // belongs to the cycle) or zero (if not) are incident.
    for (int i = 1; i < maxNode; i++) {
      expr = mip.linearNumExpr();
      for (Edge e : graph.incidentAt(i)) {
        int j = e.opposite(i);
        expr.addTerm(1.0, useEdge[i][j]);
        expr.addTerm(1.0, useEdge[j][i]);
      }
      mip.addEq(expr, mip.prod(2, useVertex[i]));
    }
    // Constraint: at any node in the cycle other than the anchor, flow out
    // is at least one unit less than flow in.
    for (int i = 1; i < maxNode; i++) {
      IloLinearNumExpr left = mip.linearNumExpr();
      IloLinearNumExpr right = mip.linearNumExpr();
      for (Edge e : graph.incidentAt(i)) {
        int j = e.opposite(i);
        left.addTerm(1.0, flow[i][j]);
        right.addTerm(1.0, flow[j][i]);
      }
      right.addTerm(-1.0, useVertex[i]);
      right.addTerm(maxNode, anchor[i]);
      mip.addLe(left, right);
    }
    // Constraint: at least one unit of flow must return to the anchor node
    // (to ensure a complete cycle).
    for (int i = 1; i < maxNode; i++) {
      expr = mip.linearNumExpr();
      for (Edge e : graph.incidentAt(i)) {
        int j = e.opposite(i);
        expr.addTerm(1.0, flow[j][i]);
      }
      mip.addGe(expr, anchor[i]);
    }
  }

  /**
   * Solves the MIP model.
   * @param timeLimit the time limit in seconds
   * @param pop the population limit (maximum number of solutions to retain)
   * @return the number of solutions found
   * @throws IloException if anything goes splat
   */
  public int solve(final double timeLimit, final int pop)
                               throws IloException {
    // Set the time limit.
    mip.setParam(IloCplex.DoubleParam.TimeLimit, timeLimit);
    // Set the population limit.
    mip.setParam(IloCplex.Param.MIP.Limits.Populate, pop);
    mip.setParam(IloCplex.Param.MIP.Pool.Intensity, INTENSITY);
    mip.populate();
    return mip.getSolnPoolNsolns();
  }

  /**
   * Gets all cycles found by the model.
   * Duplicate/equivalent cycles are filtered out, so the number found will
   * typically be less than the number reported by the solution pool.
   * @return a list of the cycles found
   * @throws IloException if CPLEX balks
   */
  public List<Path> getCycles() throws IloException {
    ArrayList<Path> cycles = new ArrayList<>();
    for (int n = 0; n < mip.getSolnPoolNsolns(); n++) {
      Path p = getCycle(n);
      // Test for equivalence to any previously found cycles.
      boolean keep = true;
      for (Path p0 : cycles) {
        if (p0.equivalentTo(p)) {
          keep = false;
          break;
        }
      }
      if (keep) {
        cycles.add(p);
      }
    }
    return cycles;
  }

  /**
   * Gets a cycle found by the model.
   * @param n the index of the solution in the pool
   * @return the cycle
   * @throws IloException if CPLEX cannot recover the solution
   */
  private Path getCycle(final int n) throws IloException {
    // Find the anchor node.
    int a = -1;
    for (int i = 1; i < maxNode; i++) {
      if (mip.getValue(anchor[i], n) > HALF) {
        a = i;
        break;
      }
    }
    // Start a path at the anchor.
    Path path = new Path(a);
    int i = a;
    // Trace the path until it returns to the anchor.
    while (!path.isCycle()) {
      // Follow the flow to the next node.
      for (Edge e : graph.incidentAt(i)) {
        int j = e.opposite(i);
        if (mip.getValue(flow[i][j], n) > HALF) {
          // Append the edge and update the current position.
          path.append(new Edge(i, j));
          i = j;
          // Check whether the path is now a cycle.
          break;
        }
      }
    }
    // Reorder and return the completed cycle.
    path.reorder();
    return path;
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    mip.close();
  }

}
